import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';

import{AngularFireModule} from 'angularfire2';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserComponent } from './user/user.component';
import { ProductsComponent } from './products/products.component';
import {  ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';





  
export const firebaseConfig = {
   apiKey: "AIzaSyAybWqWd2Dbr6A6TPi3aZF-t3l5kfSAPoo",
    authDomain: "users-352f2.firebaseapp.com",
    databaseURL: "https://users-352f2.firebaseio.com",
    projectId: "users-352f2",
    storageBucket: "users-352f2.appspot.com",
    messagingSenderId: "914352984198"
}

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'userForm', component: UsersComponent },
  { path: 'products', component: ProductsComponent },

 
  { path: '', component: UsersComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
     PageNotFoundComponent,
     UserComponent,
      UserFormComponent,
      ProductsComponent,
      ProductComponent,
   

  
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
   AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(appRoutes)

  ],
  providers: [UsersService,ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
