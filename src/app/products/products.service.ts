import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';

@Injectable()
export class ProductsService {

  productsObservable;

 constructor(private af:AngularFire) { }

 getProducts(){
    this.productsObservable = this.af.database.list('/products').delay(2000)
    return this.productsObservable;
 	}
  

  deleteProduct(product){

  let productKey = product.$key;
  this.af.database.object('/products/' + productKey).remove();
}


}
